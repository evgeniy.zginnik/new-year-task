const socket = new WebSocket('ws://vps.yojji.io', 'evgeniy_zginnik');
const switchers = document.querySelectorAll('.container input');
const levers = [false, false, false, false];
let checkedLever = 1;
let stateValue = true;
const curLever = 0;

const sendActionCheck = (lever1, lever2, stateId) => {
  const res = {
    action: 'check',
    lever1,
    lever2,
    stateId
  };

  socket.send(JSON.stringify(res));
}

const powerOff = (stateId) => {
  const res = {
    action: 'powerOff',
    stateId
  };

  socket.send(JSON.stringify(res));
}

const checkStateOfLevers = () => levers.every(lever => lever === stateValue);

const switchLever = () => {
  switchers.forEach((switcher, i) => {
    switcher.checked = levers[i];
  });
}

socket.onopen = () => console.log('connected');
socket.onclose = () => console.log('disconnected');
socket.onmessage = function(event) {
  const { pulled, stateId, newState, token, same } = JSON.parse(event.data);

  if (pulled >= 0) {
    levers[pulled] = !levers[pulled];
    switchLever();
    sendActionCheck(curLever, checkedLever, stateId);
  }

  if (newState === 'poweredOn') {
    stateValue = false;
  }

  if (newState === 'poweredOff') {
    console.log('Your token: ', token);
    socket.close();
  }

  if (same) {
    levers[checkedLever] = levers[curLever];
    checkedLever < levers.length - 1 ? checkedLever += 1 : null;
    checkStateOfLevers() ? powerOff(stateId) : null;
  }
};
